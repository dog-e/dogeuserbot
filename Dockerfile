# Copyright © 2021 Doge UserBot in Telegram
# All rights reserved.
# Licensed under the MIT License;
# you may not use this file except in compliance with the License.
FROM teledoge/dogeuserbot:latest
RUN git clone https://TeleDoge@bitbucket.org/dog-e/dogeuserbot.git /root/DOGEUSERBOT
WORKDIR /root/DOGEUSERBOT/
RUN pip3 install -r requirements.txt
CMD ["python3", "doge.py"]
