# Copyright © 2021 Doge UserBot in Telegram
# All rights reserved.
# Licensed under the MIT License;
# you may not use this file except in compliance with the License.

from telethon.errors.rpcerrorlist import YouBlockedUserError
from asyncio import sleep
from re import compile, sub
from userbot import bot
from userbot.events import register
from userbot.cmdhelp import CmdHelp

EMOJI_PATTERN = compile(
    "["
    "\U0001F1E0-\U0001F1FF"  # flags (iOS)
    "\U0001F300-\U0001F5FF"  # symbols & pictographs
    "\U0001F600-\U0001F64F"  # emoticons
    "\U0001F680-\U0001F6FF"  # transport & map symbols
    "\U0001F700-\U0001F77F"  # alchemical symbols
    "\U0001F780-\U0001F7FF"  # Geometric Shapes Extended
    "\U0001F800-\U0001F8FF"  # Supplemental Arrows-C
    "\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
    "\U0001FA00-\U0001FA6F"  # Chess Symbols
    "\U0001FA70-\U0001FAFF"  # Symbols and Pictographs Extended-A
    "\U00002702-\U000027B0"  # Dingbats
    "]+"
)

def deEmojify(inputString: str) -> str:
    return sub(EMOJI_PATTERN, "", inputString)

@register(outgoing=True, pattern="^\.[gG][aA][yY] ?(.*)")
async def gay(g):
    if g.fwd_from:
        return
    gay = g.pattern_match.group(1)
    tap = await bot.inline_query(
        "howgaybot", f"{(deEmojify(gay))}"
    )
    try:
        await tap[0].click(
            g.chat_id,
            reply_to=g.reply_to_msg_id,
            silent=True if g.is_reply else False,
            hide_via=True,
        )
    except YouBlockedUserError:
            await g.reply(f"**❗ Sanırım @howgaybot'u engellemişsiniz.\n\n💬 Engeli kaldırın ve tekrar deneyin.**")
            return
    except Exception:
        return await g.edit(
            f"**😟 Sanırım burada satır içi botlara izin verilmiyor.**"
        )
    await sleep(1)
    await g.delete()


@register(outgoing=True, pattern="^.lez ?(.*)")
async def lez(e):
    if e.fwd_from:
        return
    lez = e.pattern_match.group(1)

    botusername = "@howlezbot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, lez) 
    await tap[-2].click(e.chat_id, -1)
    await e.delete()

@register(outgoing=True, pattern="^.pedo ?(.*)")
async def pedo(e):
    if e.fwd_from:
        return
    pedo = e.pattern_match.group(1)

    botusername = "@howpedobot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, pedo) 
    await tap[0].click(e.chat_id, 0)
    await e.delete()

@register(outgoing=True, pattern="^.cock ?(.*)")
async def cock(e):
    if e.fwd_from:
        return
    cock = e.pattern_match.group(1)

    botusername = "@cocksize_bot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, cock) 
    await tap[0].click(e.chat_id, 0)
    await e.delete()

@register(outgoing=True, pattern="^.akp ?(.*)")
async def akp(e):
    if e.fwd_from:
        return
    akp = e.pattern_match.group(1)

    botusername = "@akpolcer_bot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, akp) 
    await tap[0].click(e.chat_id, 0)
    await e.delete()

@register(outgoing=True, pattern="^.kürt ?(.*)")
async def kurt(e):
    if e.fwd_from:
        return
    kurt = e.pattern_match.group(1)

    botusername = "@kurtmusunbot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, kurt) 
    await tap[0].click(e.chat_id, 0)
    await e.delete()

@register(outgoing=True, pattern="^.soylu ?(.*)")
async def soylu(e):
    if e.fwd_from:
        return
    soylu = e.pattern_match.group(1)

    botusername = "@soylusorulariyanitliyorbot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, soylu) 
    await tap[0].click(e.chat_id, 0)
    await e.delete()

@register(outgoing=True, pattern="^.nnbot ?(.*)")
async def nnbot(e):
    if e.fwd_from:
        return
    nnbot = e.pattern_match.group(1)

    botusername = "@nnbbot"
    if e.reply_to_msg_id:
        reply_to_id = await e.get_reply_message()
    tap = await bot.inline_query(botusername, nnbot) 
    await tap[0].click(e.chat_id, 0)
    await e.delete()


CmdHelp('inlinefun').add_command(
    "nbot", "<metin @kullanıcı adı/id>", "Etiketleğiniz kişiye sadece onun görebileceği bir mesaj atın.", "nnbot Nasılsın? @teledoge"
).add_command(
    "soylu", "<metin>", "Süleyman Soyluya soru sorun.", "soylu gay mısın"
).add_command(
    "gay", None, "Yüzde kaç gay olduğunuzu görün."
).add_command(
    "lez", None, "Yüzde kaç lez olduğunuzu görün."
).add_command(
    "pedo", None, "Yüzde kaç pedo olduğunuzu görün."
).add_command(
    "akp", None, "Yüzde kaç akpli olduğunuzu görün."
).add_command(
    "kürt", None, "Yüzde kaç kürt olduğunuzu görün."
).add_command(
    "cock", None, "Aletinizin tahmini uzunluğunu görün."
).add_warning(
    "Nnbbot komutunda ne yazdığınızı yöneticiler son eylemler kısmına bakarak görebilirler."
).add()