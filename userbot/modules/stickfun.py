# Copyright © 2021 Doge UserBot in Telegram
# All rights reserved.
# Licensed under the MIT License;
# you may not use this file except in compliance with the License.

from telethon.errors.rpcerrorlist import YouBlockedUserError
from random import randint, choice
from re import compile, sub
from io import BytesIO
from os import remove
from requests import get
from asyncio import sleep
from textwrap import wrap
from PIL import Image, ImageChops, ImageDraw, ImageFont
from userbot.events import register 
from userbot import bot
from userbot.cmdhelp import CmdHelp

from userbot.language import get_value
LANG = get_value("stickfun")

EMOJI_PATTERN = compile(
    "["
    "\U0001F1E0-\U0001F1FF"  # flags (iOS)
    "\U0001F300-\U0001F5FF"  # symbols & pictographs
    "\U0001F600-\U0001F64F"  # emoticons
    "\U0001F680-\U0001F6FF"  # transport & map symbols
    "\U0001F700-\U0001F77F"  # alchemical symbols
    "\U0001F780-\U0001F7FF"  # Geometric Shapes Extended
    "\U0001F800-\U0001F8FF"  # Supplemental Arrows-C
    "\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
    "\U0001FA00-\U0001FA6F"  # Chess Symbols
    "\U0001FA70-\U0001FAFF"  # Symbols and Pictographs Extended-A
    "\U00002702-\U000027B0"  # Dingbats
    "]+"
)

def deEmojify(inputString: str) -> str:
    return sub(EMOJI_PATTERN, "", inputString)

@register(outgoing=True, pattern="^\.[wW][sS][cC](?: |$)(.*)")
async def waifu(w):
    if w.fwd_from:
        return
    text = w.pattern_match.group(1)
    if not text:
        if w.is_reply:
            text = (await w.get_reply_message()).message
        else:
            await w.answer(f"**🐾 {LANG['REQUEST']}**")
            return
    animes = [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
        32,
        33,
        34,
        35,
        36,
        37,
        38,
        39,
        40,
        41,
        42,
        43,
        44,
        45,
        46,
        47,
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
        58,
        59,
        60,
        61,
        62,
        63,
    ]
    tap = await bot.inline_query(
        "stickerizerbot", f"#{choice(animes)}{(deEmojify(text))}"
    )
    try:
        await tap[0].click(
            w.chat_id,
            reply_to=w.reply_to_msg_id,
            silent=True if w.is_reply else False,
            hide_via=True,
        )
    except YouBlockedUserError:
            await w.reply(f"**❗ {LANG['BLOCKED_WAIFU']}\n\n💬 {LANG['SUGGEST']}**")
            return
    except Exception:
        return await w.edit(
            f"**😟 {LANG['ERRWGROUP']}**"
        )
    await sleep(1)
    await w.delete()

# Thanks to t.me/qeuka honka stickers for helps.

@register(outgoing=True, pattern="^\.[hH][sS][cC](?: |$)(.*)")
async def honka(h):
    if h.fwd_from:
        return
    honka = h.pattern_match.group(1)
    if not honka:
        if h.is_reply:
            honka = (await h.get_reply_message()).message
        else:
            await h.answer(f"**🐸 {LANG['REQUEST']}**")
            return

    tap = await bot.inline_query(
        "honka_says_bot", f"{(deEmojify(honka))}."
    )
    try:
        await tap[0].click(
            h.chat_id,
            reply_to=h.reply_to_msg_id,
            silent=True if h.is_reply else False,
            hide_via=True,
        )
    except YouBlockedUserError:
            await h.reply(f"**❗ {LANG['BLOCKED_HONKA']}\n\n💬 {LANG['SUGGEST']}**")
            return
    except Exception:
        return await h.edit(
            f"**😟 {LANG['ERRWGROUP']}**"
        )
    await sleep(1)
    await h.delete()


# Thanks to TeamDerUntergang amongus stickers for idea and helps.    ~t.me/SedenUserBot

@register(outgoing=True, pattern="^.[aA][sS][cC](?: |$)(.*)")
async def usus(a):
    sticktext = a.pattern_match.group(1).strip()

    if len(sticktext) < 1:
        await a.edit(f"**ඞ {LANG['REQUEST']}**")
        return

    arr = randint(1, 12)
    fontsize = 120
    FONT_FILE = 'userbot/fonts/modern.ttf'
    url = 'https://bitbucket.org/dog-e/doge/raw/FILES/Resources/StickFun/AmongUs/'
    font = ImageFont.truetype(FONT_FILE, size=int(fontsize))

    usus = Image.open(BytesIO(get(f'{url}{arr}.png').content))
    stick_text = '\n'.join(['\n'.join(wrap(part, 30)) for part in sticktext.split('\n')])
    w, h = ImageDraw.Draw(Image.new('RGB', (1, 1))).multiline_textsize(
        stick_text, font, stroke_width=2
    )
    text = Image.new('RGBA', (w + 40, h + 40))
    ImageDraw.Draw(text).multiline_text(
        (15, 15), stick_text, '#FFF', font, stroke_width=2, stroke_fill='#000'
    )
    w = usus.width + text.width + 30
    h = max(usus.height, text.height)
    image = Image.new('RGBA', (w, h))
    image.paste(usus, (0, h - usus.height), usus)
    image.paste(text, (w - text.width, 0), text)
    image.thumbnail((512, 512))
    image.save("sus.webp", 'WebP')

    await a.delete()
    await a.client.send_file(a.chat_id, "sus.webp", reply_to=a.message.reply_to_msg_id)

    try:
        remove(FONT_FILE)
    except:
        pass


@register(outgoing=True, pattern="^.[dD][sS][cC](?: |$)(.*)")
async def doge(event):
    sticktext = event.pattern_match.group(1).strip()

    if len(sticktext) < 1:
        await event.edit(f"**🐶 {LANG['REQUEST']}**")
        return

    arr = randint(1, 15)
    fontsize = 120
    FONT_FILE = 'userbot/fonts/modern.ttf'
    url = 'https://bitbucket.org/dog-e/doge/raw/FILES/Resources/StickFun/Doge/'
    font = ImageFont.truetype(FONT_FILE, size=int(fontsize))

    doge = Image.open(BytesIO(get(f'{url}{arr}.png').content))
    stick_text = '\n'.join(['\n'.join(wrap(part, 30)) for part in sticktext.split('\n')])
    w, h = ImageDraw.Draw(Image.new('RGB', (1, 1))).multiline_textsize(
        stick_text, font, stroke_width=2
    )
    text = Image.new('RGBA', (w + 40, h + 40))
    ImageDraw.Draw(text).multiline_text(
        (15, 15), stick_text, '#FFF', font, stroke_width=2, stroke_fill='#000'
    )
    w = doge.width + text.width + 30
    h = max(doge.height, text.height)
    image = Image.new('RGBA', (w, h))
    image.paste(doge, (0, h - doge.height), doge)
    image.paste(text, (w - text.width, 0), text)
    image.thumbnail((512, 512))
    image.save("hav.webp", 'WebP')

    await event.delete()
    await event.client.send_file(event.chat_id, "hav.webp", reply_to=event.message.reply_to_msg_id)

    try:
        remove(FONT_FILE)
    except:
        pass


@register(outgoing=True, pattern="^.[rR][gG][bB](?: |$)(.*)")
async def rgb(event):
    R = randint(0,256)
    G = randint(0,256)
    B = randint(0,256)

    # Giriş metnini al
    sticktext = event.pattern_match.group(1).strip()

    if len(sticktext) < 1:
        await event.edit(f"**🌈 {LANG['REQUEST']}**")
        return

    # https://docs.python.org/3/library/textwrap.html#textwrap.wrap
    sticktext = wrap(sticktext, width=10)
    # Listeyi bir dizeye dönüştür
    sticktext = '\n'.join(sticktext)

    image = Image.new("RGBA", (512, 512), (255, 255, 255, 0))
    draw = ImageDraw.Draw(image)
    fontsize = 230

    FONT_FILE = 'userbot/fonts/modern.ttf'

    font = ImageFont.truetype(FONT_FILE, size=int(fontsize))

    while draw.multiline_textsize(sticktext, font=font) > (512, 512):
        fontsize -= 10
        font = ImageFont.truetype(FONT_FILE, size=int(fontsize))

    width, height = draw.multiline_textsize(sticktext, font=font)
    draw.multiline_text(((512-width)/2,(512-height)/2), sticktext, font=font, fill=(R, G, B))

    image_stream = BytesIO()
    image_stream.name = "@rgb.webp"

    def trim(im):
        bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
        diff = ImageChops.difference(im, bg)
        diff = ImageChops.add(diff, diff, 2.0, -100)
        bbox = diff.getbbox()
        return im.crop(bbox) if bbox else im

    image = trim(image)
    image.save(image_stream, "WebP")
    image_stream.seek(0)

    # mesajı sil
    await event.delete()

    await event.client.send_file(event.chat_id, image_stream, reply_to=event.message.reply_to_msg_id)
    # Temizlik
    try:
        remove(FONT_FILE)
    except:
        pass


CmdHelp('stickfun').add_command(
    'wsc', '<metin>', 'Metninizi Waifu anime çıkartmalarına dönüştürün.', 'wsc Selam?'
).add_command(
    'hsc', '<metin>', 'Metninizi Honka çıkartmalarına dönüştürün.', 'hsc Selam, naber?'
).add_command(
    'asc', '<metin>', 'Metninizi Among Us çıkartmalarına dönüştürün.', 'asc İyiyim, sen?'
).add_command(
    'dsc', '<metin>', 'Metninizi Doge çıkartmalarına dönüştürün.', 'dsc Ben de iyiyim.'
).add_command(
    'rgb', '<metin>', 'Metninizi rastgele bir renkte çıkartmaya dönüştürün.', 'rgb Hep iyi ol!'
).add()