# Copyright © 2021 Doge UserBot in Telegram
# All rights reserved.
# Licensed under the MIT License;
# you may not use this file except in compliance with the License.

from userbot import CMD_HELP, ASYNC_POOL, CONSOLE_LOGGER_VERBOSE, LOGSPAMMER, tgbot, SPOTIFY_DC, G_DRIVE_CLIENT_ID, lastfm, GENIUS, YOUTUBE_API_KEY, OPEN_WEATHER_MAP_APPID, AUTO_PP, REM_BG_API_KEY, OCR_SPACE_API_KEY, PM_AUTO_BAN, ANTI_SPAMBOT, OTOMATIK_KATILMA, BOTLOG_CHATID, DOGE_VERSION
from userbot.events import register
from telethon import version
from platform import python_version
from userbot.cmdhelp import CmdHelp

# ██████ LANGUAGE CONSTANTS ██████ #

from userbot.language import get_value
LANG = get_value("status")

# ████████████████████████████████ #

def durum(s):
    if s == None:
        return "❌"
    else:
        if s == False:
            return "❌"
        else:
            return "✅"

@register(outgoing=True, pattern="^.[Dd][Uu][Rr][Uu][Mm]|^.[sS]tatus")
async def durums(event):

    await event.edit(f"""
**🐶 Doge {LANG['VERSION']}:**     `{DOGE_VERSION}`
**🧩 Python {LANG['VERSION']}:**   `{python_version()}`
**🧩 TeleThon {LANG['VERSION']}:** `{version.__version__}` 
**🧩 Plugin:**   `{len(CMD_HELP)}` 

`{durum(tgbot)}` **Inline Bot**
`{durum(BOTLOG_CHATID)}` **BOTLOG**
`{durum(LOGSPAMMER)}` **{LANG['LOGSSPAMMER']}**
`{durum(CONSOLE_LOGGER_VERBOSE)}` **{LANG['CONSOLELOGGER']}**
`{durum(SPOTIFY_DC)}` **Spotify**
`{durum(G_DRIVE_CLIENT_ID)}` **Google Drive**
`{durum(lastfm)}` **LastFm**
`{durum(GENIUS)}` **Genius**
`{durum(YOUTUBE_API_KEY)}` **YouTube API**
`{durum(OPEN_WEATHER_MAP_APPID)}` **OpenWeather**
`{durum(AUTO_PP)}` **{LANG['AUTOPP']}**
`{durum(REM_BG_API_KEY)}` **RemoveBG**
`{durum(OCR_SPACE_API_KEY)}` **OcrSpace**
`{durum(PM_AUTO_BAN)}` **PM AutoBan**
`{durum(ANTI_SPAMBOT)}` **Anti Spam Bot**
`{durum(OTOMATIK_KATILMA)}` **{LANG['AUTOJOIN']}***

**✅ {LANG['OK']}**
    """)

CmdHelp('durum').add_command(
    'durum', None, 'Eklenen API\'ler ve sürümleri gösterir.'
).add()
