<p align="center"><a href="https://t.me/DogeUserBot"><img src="https://bitbucket.org/dog-e/doge/raw/FILES/Resources/dog-el.jpg" width="400"></a></p>
  <h1 align="center">🐶 DOGE USERBOT 🐾</h1>
</p>
<p align="center">
    ❤️ Telegram'da bir köpeğiniz olsun!
    <br>
    ❤️ Have a dog in Telegram!
    <br>
    <br>
        <a href="https://t.me/DogeUserBot">📣 Güncellemeler | Updates 📣</a>
    <br>
    <br>
        <a href="https://t.me/DogeSup">💬 Destek Grubu | Support Group 💬</a>
    <br>
    <br>
        <a href="https://t.me/DogePlugin">🧩 Pluginler | Plugins 🧩</a>
    <br>
    <br>
        <a href="https://bitbucket.org/dog-e/dogeuserbot/wiki/">⚡ Kurlumlar | Installations ⚡</a>
</p>

----

## 🔗 Kolay Kurulumlar | Easy Setups

### 🌐 Online Kurulum | Online Setup

⬇️ Aşağıdaki butona tıklayın/dokunun:

[![Run on Repl.it](https://repl.it/badge/github/@DogeUserBot/DogeOnline)](https://)

----

### 🤖 Android

📲 [Termux uygulamasını "buradan" indirin,](https://)

📋 Aşağıdaki kodu Termux'a yapıştırın:

```bash <(curl -L https://)```

----

### 🍎 IOS

📲 [TestFlight uygulamasını "buradan" indirin,](https://)

📲 [ISH uygulamasını "buradan" indirip 'Start Testing'e dokunun,](https://)

📋 Aşağıdaki kodu ISH'a yapıştırın:

```apk update && apk add bash && apk add curl && curl -L -o doge_installer.sh https:// && chmod +x doge_installer.sh && bash doge_installer.sh```

----

### 💻 Windows 10

🖥️ [Python 3.9'u "buradan" indirin,](https://)

📋 Aşağıdaki kodu PowerShell'e yapıştırın:

```Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://')```

----

## 🔗 Zor Kurulumlar | Diffucult Setups

### 🌐 Elle Kurulum | Manuel Setup

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://https://bitbucket.org/dog-e/dogeuserbot/src/DOGE)

----

### ℹ️ Terminal, Termux, vb.

```sh
git clone https://
cd installer
pip install -r requirements.txt
python3 -m doge_installer
```

----

## 🪧 Örnek Plugin | Example Plugin

```python
from userbot.events import register
from userbot.cmdhelp import CmdHelp 
# <-- Bunu ekleyin ve daha sonra herhangi bir metin yazabilirsiniz.

@register(outgoing=True, pattern="^.ornekdeneme")
async def ornekdeneme(event):
    await event.edit('Bu bir örnek deneme pluginidir!')

Help = CmdHelp('deneme') # Bilgi ekleyeceğiz.
Help.add_command('ornekdeneme', # Komutu bu şekilde yazıyoruz.
    None, # Komut parametresi varsa yazın yoksa None yazın.
    'Deneme yapıyor.', # Komut açıklamasını bu şekilde belirtiyoruz.
    'ornekdeneme' # Örnek komut kullanımını burada belirtiyoruz.
    )
Help.add_info('@DogeUserBot için yapıldı.') # Buna benzer bilgi ekleyebilirsiniz.
Help.add_warning('BUNU YAPMA!') # Buraya uyarı ekleyebilirsiniz.
Help.add() # Plugini bu şekilde bitiriyoruz.
```

----

## 💬 Bilgilendirme | Information

📍 Herhangi bir soru veya geribildirim için bize [🐶 Doge Destek grubumuzdan](https://t.me/DogeSup) ulaşabilirsiniz.

```
ℹ️ Doge UserBot, açık kaynaklı bir projedir.

💡 UserBot kötüye kullanım sebebiyle;
    
   🚫 Telegram hesabınız kısıtlanabilir/yasaklanabilir.
    
   💣 Her yaptığınız işlemden sorumlu tutulursunuz.
    
   ⛔️ Doge UserBot yöneticileri hiçbir sorumluluk kabul etmemektedir.
    
   📍 Doge UserBot kurarak tüm bu sorumlulukları kabul etmiş olursunuz.
```

### 📝 Lisans | License

<a href="https://tr.m.wikipedia.org/wiki/GNU_Genel_Kamu_Lisans%C4%B1#"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/1280px-GPLv3_Logo.svg.png" width="150"></a>

🔐 Bu proje GPL-3.0 lisansı ile korunmaktadır.

✅ Tüm hakları saklıdır.


### 🤍 Teşekkürler! | Thanks!

🧑‍💻 [Yusuf Usta](https://github.com/yusufusta)

----
